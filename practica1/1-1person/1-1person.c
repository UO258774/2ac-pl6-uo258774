#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Javier;
    PPerson pJavier;
    // Memory location of Javier variable is assigned to the pointer
    pJavier = &Javier;
    Javier.heightcm = 180;
    Javier.weightkg = 84.0;
    pJavier->weightkg = 83.2;
    return 0;

    Person Peter;
    strcpy(Peter.name, "Peter");
    Peter.heightcm = 175;
    // Assign the weight
    Peter.weightkg = 78.7;
    // Show the information of the Peter data structure on the screen
    printf("%s's height: %d; %s's weight: %f\n",Peter.name, Peter.heightcm, Peter.name, Peter.weightkg);
    return 0;
}
